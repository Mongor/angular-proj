import { Component, OnInit } from '@angular/core';
import { NoticeService } from '../services/notice.service';
import { UserService } from '../services/user.service';
import { error } from 'protractor';

@Component({
	selector: 'app-admin-page',
	templateUrl: './admin-page.component.html',
	styleUrls: ['./admin-page.component.less']
})
export class AdminPageComponent implements OnInit {

	allNotices: any;
	noticesCount: number;

	constructor(private noticeService: NoticeService, private userService: UserService) { }

	ngOnInit() {

		this.getAllNotice();

	}

	getAllNotice() {
		this.noticeService.getUnpublishedNotice()
			.subscribe(
				data => {
					this.allNotices = data;

					this.noticesCount = this.allNotices.length;
				},
				error => {
				}
			)
	}

	updataStatus(noticeId, pubStatus){

		const data = {
			id: noticeId,
			published: pubStatus
		}

		this.noticeService.update(noticeId, data)
			.subscribe(
				response => {
					this.resreshPage();

				},
				error => {

				}
			)
	}

	deleteNotice(noticeId){
		this.noticeService.delete(noticeId)
			.subscribe(
				response => {
					this.resreshPage();

				},error => {

				}
			)
	}

	resreshPage() {
		this.getAllNotice();
	}

}
