import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayNoticeBlockComponent } from './pay-notice-block.component';

describe('PayNoticeBlockComponent', () => {
  let component: PayNoticeBlockComponent;
  let fixture: ComponentFixture<PayNoticeBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayNoticeBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayNoticeBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
