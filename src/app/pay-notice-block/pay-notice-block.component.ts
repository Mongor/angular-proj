import { Component, OnInit } from '@angular/core';
import { NoticeService } from '../services/notice.service';

@Component({
	selector: 'app-pay-notice-block',
	templateUrl: './pay-notice-block.component.html',
	styleUrls: ['./pay-notice-block.component.less']
})
export class PayNoticeBlockComponent implements OnInit {

	noticesPay: any;
	currentNotice = null;
	currentIndex = -1;

	constructor(private noticebeService: NoticeService) { }

	ngOnInit() {
		this.retrieveNotices();
	}

	retrieveNotices() {
		this.noticebeService.getAllPayed()
			.subscribe(
				data => {
					this.noticesPay = data;
				},
				error => {
				});
	}

	refreshList() {
		this.retrieveNotices();
		this.currentNotice = null;
		this.currentIndex = -1;
	}

	setActiveNotice(noticesPay, index) {
		this.currentNotice = noticesPay;
		this.currentIndex = index;
	}

}
