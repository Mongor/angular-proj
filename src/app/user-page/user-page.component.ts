import { Component, OnInit } from '@angular/core';
import { UserService } from "../services/user.service";
import { NoticeService } from "../services/notice.service";

import {
	FormGroup,
	FormControl,
	Validators
} from "@angular/forms";


@Component({
	selector: 'app-user-page',
	templateUrl: './user-page.component.html',
	styleUrls: ['./user-page.component.less']
})


export class UserPageComponent implements OnInit {

	updateUser: FormGroup;
	passwordForm: FormGroup;
	crateNoticeFrom: FormGroup;

	isUserUpdata: boolean = false;
	isUserNoticeCreate: boolean = false;
	currentUser: any = 1;
	noticeCount: number = 0;
	id = 1;

	newPassword: any;

	user_name: string;
	user_phone: number;
	phoneValidation = /^[0-9]{10}$/;
	passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;


	notice_title: string;
	notice_text: string;
	notice_email: string;
	notice_phone: number;

	new_password: string;
	re_new_password: string;

	get controlsUser() {
		return this.updateUser.controls;
	}

	constructor(private userService: UserService, private noticeService: NoticeService) { }

	ngOnInit() {

		this.updateUser = new FormGroup({
			'user_name': new FormControl('', [Validators.required, Validators.minLength(3)]),
			'user_phone': new FormControl('', [Validators.required, Validators.minLength(10), Validators.pattern(this.phoneValidation)])
		});

		this.crateNoticeFrom = new FormGroup({
			'notice_title': new FormControl('', [Validators.required, Validators.minLength(5)]),
			'notice_text': new FormControl('', [Validators.required, Validators.minLength(120)])
		});

		this.passwordForm = new FormGroup({
			'new_password': new FormControl('', [Validators.required, Validators.minLength(5), Validators.pattern(this.passwordPattern)]),
			're_new_password': new FormControl('', [Validators.required, Validators.minLength(5), Validators.pattern(this.passwordPattern)])
		});

		this.getUser(this.id);

	}

	getUser(id) {
		this.userService.get(id)
			.subscribe(
				data => {
					this.currentUser = data;
					this.noticeCount = this.currentUser.notices.length;
				},
				error => {
				}
			)
	}

	createUserNotice(){

		const newUserNotice = {
			title : this.crateNoticeFrom.value.notice_title,
			text : this.crateNoticeFrom.value.notice_text,
			userId : this.id
		}


		this.noticeService.createUserNotice(newUserNotice)
			.subscribe(
				response => {
					this.crateNoticeFrom.reset();
					this.resreshUser();
					return this.isUserNoticeCreate = true;
				},
				error => {
				}
			)
	}

	onUpdataUser() {

		const newUserData = {
			name: this.updateUser.value.user_name,
			phone: this.updateUser.value.user_phone
		}


		this.userService.update(1, newUserData)
			.subscribe(
				response => {
					this.isUserUpdata = true;
				},
				error => {
					this.isUserUpdata = false;
				}
			)
	}


	changePassword() {

		const newPassword = {
			password: this.passwordForm.value.new_password
		}

		this.new_password = this.passwordForm.value.new_password;
		this.re_new_password = this.passwordForm.value.re_new_password;


		if (this.new_password != this.re_new_password) {

			return newPassword.password = null;

		} else {

			this.userService.update(1, newPassword)
				.subscribe(
					response => {
					},
					error => {
					}
				)
		}
	}

	deleteNotice(noticeId) {
		this.noticeService.delete(noticeId)
			.subscribe(
				response => {

					this.resreshUser();
				},
				error => {
				}
			)
	}

	updataNoticeTime(noticeId) {

		const date = {
			id: noticeId,
			createdAt: new Date(),
			updatedAt: new Date()
		}


		this.noticeService.update(noticeId, date)
			.subscribe(
				response => {
					
					this.resreshUser();
				},
				error => {
				}
			)
	}

	resreshUser() {
		this.getUser(this.id);
	}


	panelOpenState = false;

}
