import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { NoticeComponent } from './notice/notice.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { UserPageComponent } from './user-page/user-page.component';
import { NotFoundComponent } from './not-found/not-found.component';


const routes: Routes = [
  {
    path: '' ,
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home' ,
    component: HomeComponent,
    pathMatch: 'full'
  },
  {
    path: 'home/published/:id',
    component: NoticeComponent
  },
  {
    path: 'login',
    component: LoginComponent,
    pathMatch: 'full'
  },
  {
    path: 'register',
    component: RegisterComponent,
    pathMatch: 'full'
  },
  {
    path: 'admin',
    component: AdminPageComponent,
    pathMatch: 'full'
  },
  {
    path: 'user',
    component: UserPageComponent,
    pathMatch: 'full'
  },
  { 
    path: '**',
    component: NotFoundComponent 
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})


export class AppRoutingModule { }
