import { Component, OnInit, Input } from '@angular/core';
import { SubscribeService } from "../services/subscribe.service";
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

import {
	FormGroup,
	FormControl,
	Validators
} from "@angular/forms";

@Component({
	selector: 'app-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.less']
})


export class FooterComponent implements OnInit {
	snackbarClass = 'custom-class';

	submitForm: FormGroup;
	emailPattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
	@Input() submitEmail: any;

	get email() {
		return this.submitForm.get("email");
	}

	constructor(private subscribeService: SubscribeService , private _snackBar: MatSnackBar) { }

	ngOnInit() {

		this.submitForm = new FormGroup({
			'email': new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern(this.emailPattern)])
		});

	}


	submitAction() {

		const data = {
			email: this.submitForm.value
		};

		this.subscribeService.create(data)
			.subscribe(
				response => {

					this.snackbarClass = 'success-class'
					this.openSnackBar( `Thanks for subscription:  ${this.submitEmail}`, null);
					this.submitForm.reset();

				},
				error => {

					this.snackbarClass = 'fail-class';
					this.openSnackBar( `This email is subscribed:  ${this.submitEmail}`, null);

				});

		this.submitEmail = data.email.email ;
	
	}

	openSnackBar(message: string, action: string) {
		const config = new MatSnackBarConfig();

		this._snackBar.open(message, action, {
			duration: 3000,
			panelClass: [this.snackbarClass]
		});
	}

}
