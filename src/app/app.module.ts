import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';

import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatGridListModule,
  MatDialogModule,
  MatBadgeModule,
  MatCheckboxModule,
  MatExpansionModule } from '@angular/material';
import {MatMenuModule} from '@angular/material/menu';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NoticeComponent } from './notice/notice.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AllNoticeBlockComponent } from './all-notice-block/all-notice-block.component';
import { PayNoticeBlockComponent } from './pay-notice-block/pay-notice-block.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RestorePasswordDialog } from './login/login.component';
import { UserPageComponent } from './user-page/user-page.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    NoticeComponent,
    HeaderComponent,
    FooterComponent,
    AllNoticeBlockComponent,
    PayNoticeBlockComponent,
    HomeComponent,
    LoginComponent,
    RestorePasswordDialog,
    UserPageComponent,
    AdminPageComponent,
    NotFoundComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxPaginationModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    MatDialogModule,
    MatBadgeModule,
    MatExpansionModule,
    MatCheckboxModule,
    MatMenuModule,
    MatSnackBarModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ], 
  entryComponents: [
    RestorePasswordDialog
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
