import { Component, OnInit, Input } from '@angular/core';
import { UserService } from "../services/user.service";
import { Router } from '@angular/router';


import {
	FormGroup,
	FormControl,
	Validators
} from "@angular/forms";

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.less']
})

export class RegisterComponent implements OnInit {

	registerForm: FormGroup;

	registerStatus: boolean = false;
	errorStatus: boolean = false;

	emailPattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
	passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;

	registerEmail: string;
	registerPassword: string;
	
	get controls() {
		return this.registerForm.controls;
	}

	constructor(public userService: UserService , private router: Router) { }

	ngOnInit() {

		this.registerForm = new FormGroup({

			'registerEmail': new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern(this.emailPattern)]),
			'registerPassword': new FormControl('', [Validators.required, Validators.minLength(5), Validators.pattern(this.passwordPattern)])

		});

	}


	registerAction() {


		const data = {

			email: this.registerForm.value.registerEmail,
			password: this.registerForm.value.registerPassword

		};

		this.userService.create(data)
			.subscribe(
				response => {

					this.registerForm.reset();

					this.registerStatus = true;
					this.errorStatus = false;
					
					this.router.navigate(['/login']);
				},
				error => {

					this.registerStatus = false;
					this.errorStatus = true;
				});

	}
}