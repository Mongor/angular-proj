import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserService } from "../services/user.service";


import {
	FormGroup,
	FormControl,
	Validators
} from "@angular/forms";

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.less']
})

export class LoginComponent implements OnInit {

	loginForm: FormGroup;

	registerStatus: boolean = false;
	errorStatus: boolean = false;



	emailPattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
	passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
	

	loginEmail: string;
	loginPassword: string;

	get controls() {
		return this.loginForm.controls;
	}

	constructor( public userService: UserService, public dialog: MatDialog) { }

	ngOnInit() {

		this.loginForm = new FormGroup({

			'loginEmail': new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern(this.emailPattern)] ),
			'loginPassword': new FormControl('', [Validators.required, Validators.minLength(5) ,Validators.pattern(this.passwordPattern)] )
			
		});

	}


	loginRegisterAction() {


		const data = {

			email: this.loginForm.value.loginEmail,
			password: this.loginForm.value.loginPassword

		};
	
	}




	openDialog() {
		const dialogRef = this.dialog.open(RestorePasswordDialog);
	}

}


@Component({
	selector: 'restore-password-dialog',
	template: `
		<div class="restorePasswordModal d-flex align-items-center">
			<form class="d-flex flex-column justify-content-start w-100" (ngSubmit)="restorePassword()" [formGroup]="restorePasswordForm" action="" method="POST">

				<button class="closeRestoreModal" mat-button mat-dialog-close>
					<mat-icon class="closeIcon">close</mat-icon>
				</button>

				<p class="restoreText">Restore details will be delivery until 15 minutes.</p>

				<span *ngIf="!restorePasswordForm.get('emailForRestore').valid && restorePasswordForm.get('emailForRestore').touched" class="needDataToEnterClass mb-2">Email is not correct...</span>

				<mat-form-field appearance="outline">
					<mat-label>Enter email:</mat-label>
					<input matInput placeholder="email@gmail.com" formControlName="emailForRestore" type="email" required>
				</mat-form-field>

				<button class="sendEmailBtn" mat-button [disabled]="!restorePasswordForm.valid">Send email</button>

			</form>
		</div>
  	`
})
export class RestorePasswordDialog implements OnInit {

	restorePasswordForm: FormGroup;

	emailPattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;

	@Input() emailForRestore: any;

	get email() {
		return this.restorePasswordForm.get("emailForRestore");
	}

	constructor() { }

	ngOnInit() {

		this.restorePasswordForm = new FormGroup({
			'emailForRestore': new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern(this.emailPattern)])
		});

	}

}
