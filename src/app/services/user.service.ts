import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const allUsers = 'http://localhost:8080/users';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(allUsers);
  }

  get(id) {
    return this.http.get(`${allUsers}/${id}`);
  }

  create(data) {
    return this.http.post(allUsers, data);
  }

  update(id, data) {
    return this.http.put(`${allUsers}/${id}`, data);
  }

  delete(id) {
    return this.http.delete(`${allUsers}/${id}`);
  }

}