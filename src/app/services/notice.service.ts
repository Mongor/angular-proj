import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const publishedNoticeUrl = 'http://localhost:8080/notice/published';

const unpublishedNotice = 'http://localhost:8080/notice/unpublished';

const allNoticeForUser = 'http://localhost:8080/notice';

const publishedPayedNoticeUrl = 'http://localhost:8080/notice/payed';


@Injectable({
	providedIn: 'root'
})

export class NoticeService {

	constructor(private http: HttpClient) { }

	getAll() {
		return this.http.get(publishedNoticeUrl);
	}

	getUnpublishedNotice() {
		return this.http.get(unpublishedNotice);
	}

	getAllPayed() {
		return this.http.get(publishedPayedNoticeUrl);
	}

	get(id) {
		return this.http.get(`${publishedNoticeUrl}/${id}`);
	}

	create(data) {
		return this.http.post(publishedNoticeUrl, data);
	}

	createUserNotice(data) {
		return this.http.post(allNoticeForUser, data);
	}

	update(id, data) {
		return this.http.put(`${allNoticeForUser}/${id}`, data);
	}

	delete(id) {
		return this.http.delete(`${allNoticeForUser}/${id}`);
	}

}