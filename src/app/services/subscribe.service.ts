import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const subscribesUrl = 'http://localhost:8080/subscribes';

@Injectable({
    providedIn: 'root'
})
export class SubscribeService {

    constructor( private http: HttpClient ) { }

    create(data){
        return this.http.post(subscribesUrl, data.email);
    }

}
