import { Component,OnInit } from '@angular/core';
import { NoticeService } from '../services/notice.service';

@Component({
	selector: 'app-all-notice-block',
	templateUrl: './all-notice-block.component.html',
	styleUrls: ['./all-notice-block.component.less']
})
export class AllNoticeBlockComponent implements OnInit {

	p: number = 1;

	notices: any;

	currentNotice = null;
	currentIndex = -1;


	constructor(private noticebeService: NoticeService ) { }

	ngOnInit() {
		this.retrieveNotices();
	}


	retrieveNotices() {
		this.noticebeService.getAll()
			.subscribe(
				data => {
					this.notices = data;
				},
				error => {
					console.log(error);
				});
	}

	refreshList() {
		this.retrieveNotices();
		this.currentNotice = null;
		this.currentIndex = -1;
	}

	setActiveNotice(notice, i) {
		this.currentNotice = notice;
		this.currentIndex = i;
	}
}
