import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllNoticeBlockComponent } from './all-notice-block.component';

describe('AllNoticeBlockComponent', () => {
  let component: AllNoticeBlockComponent;
  let fixture: ComponentFixture<AllNoticeBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllNoticeBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllNoticeBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
