import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NoticeService } from '../services/notice.service';


@Component({
	selector: 'app-notice',
	templateUrl: './notice.component.html',
	styleUrls: ['./notice.component.less']
})
export class NoticeComponent implements OnInit {

	navigationSubscription;

	currentNotice: any;

	constructor(private noticeService: NoticeService, private route: ActivatedRoute, private router: Router) { 
		this.navigationSubscription = this.router.events.subscribe((e: any) => {
			if (e instanceof NavigationEnd) {
				this.initialiseInvites();
			}
		});
	}

	initialiseInvites() {
		this.getNotice(this.route.snapshot.paramMap.get('id'));
	}

	ngOnInit() {}

	getNotice(id) {
		this.noticeService.get(id)
			.subscribe(
				data => {
					this.currentNotice = data;
					console.log(this.currentNotice);

				},
				error => {
					console.log(error);
				});
	}

	ngOnDestroy() {
		if (this.navigationSubscription) {
			this.navigationSubscription.unsubscribe();
		}
	}




}
